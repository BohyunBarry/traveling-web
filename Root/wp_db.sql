-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 29, 2016 at 09:22 PM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wp_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `add_delete_record`
--

CREATE TABLE `add_delete_record` (
  `id` int(11) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `add_delete_record`
--

INSERT INTO `add_delete_record` (`id`, `content`) VALUES
(1, 'Thanks\n'),
(2, 'Hope you can get improvement\n');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) UNSIGNED NOT NULL,
  `full_name` varchar(150) NOT NULL,
  `email` varchar(200) NOT NULL,
  `comment` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `up` int(11) UNSIGNED NOT NULL,
  `down` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `full_name`, `email`, `comment`, `date`, `active`, `up`, `down`) VALUES
(8, 'Bob', 'email@mail.com', 'About this web Application: the part of "Join Us". Do a rate, tell me how you feel.', '2016-04-16 12:58:59', 1, 0, 0),
(9, 'Paker', 'email@mail.com', 'About this web Application: the part of "Explored". Do a rate, tell me how you feel.', '2016-04-16 12:59:00', 1, 0, 0),
(10, 'Batman', 'email@mail.com', 'About this web Application: the part of "Contact Us". Do a rate, tell me how you feel.', '2016-04-16 12:59:01', 1, 0, 0),
(11, 'Spiderman', 'email@mail.com', 'About this whole web ApplicationDo a rate, tell me how you feel.', '2016-04-16 12:59:03', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE `info` (
  `id` int(11) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `tech` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `info`
--

INSERT INTO `info` (`id`, `fname`, `lname`, `tech`, `email`, `address`) VALUES
(147, 'Nilesh', 'Shirgave', 'France', 'nilesh@domain.com', 'Kolhapur'),
(148, 'Sandip', 'Patil', 'London', 'sandip@domain.com', 'Kolhapur'),
(149, 'Amit', 'Patil', 'China', 'amit@domain.com', 'Karad'),
(150, 'bb', 'ff', 'Janpan', 'bb@domain.com', 'kara');

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` int(11) UNSIGNED NOT NULL,
  `user` varchar(15) NOT NULL,
  `item` int(11) UNSIGNED NOT NULL,
  `rate` tinyint(1) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `s281_items_cmts`
--

CREATE TABLE `s281_items_cmts` (
  `c_id` int(11) NOT NULL,
  `c_item_id` int(12) NOT NULL DEFAULT '0',
  `c_ip` varchar(20) DEFAULT NULL,
  `c_name` varchar(64) DEFAULT '',
  `c_text` text NOT NULL,
  `c_when` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s281_items_cmts`
--

INSERT INTO `s281_items_cmts` (`c_id`, `c_item_id`, `c_ip`, `c_name`, `c_text`, `c_when`) VALUES
(1, 3, '::1', 'nn', 'njn', 1460832497),
(2, 0, '::1', 'ggf', 'ghf', 1460832664),
(3, 0, '::1', 'ggf', 'ghf', 1460832665),
(4, 0, '::1', 'ggf', 'ghf', 1460832667),
(5, 1, '::1', '22', 'nkjb', 1460833230),
(6, 2, '::1', 'bj', 'jbj', 1460833249),
(7, 0, '::1', '55', 'jnk', 1460833318),
(8, 0, '::1', 'bn', 'nn', 1460833516),
(9, 10, '::1', 'nj', 'hgy', 1460922465),
(10, 0, '::1', 'nn ', '  n', 1461068760),
(11, 4, '::1', 'bgch', 'xfdx', 1461069089),
(12, 0, '::1', 'bbb', 'lllll\n', 1461313797),
(13, 0, '::1', 'hksgadk', 'hjcgsayugsauy', 1461599259),
(14, 21, '::1', 'bob', 'nice\n', 1461783812),
(15, 24, '::1', 'Zack', 'Opera House', 1461783948),
(16, 23, '::1', 'Sarah', 'beautifu', 1461868498);

-- --------------------------------------------------------

--
-- Table structure for table `s281_photos`
--

CREATE TABLE `s281_photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT '',
  `filename` varchar(255) DEFAULT '',
  `description` text NOT NULL,
  `when` int(11) NOT NULL DEFAULT '0',
  `comments_count` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s281_photos`
--

INSERT INTO `s281_photos` (`id`, `title`, `filename`, `description`, `when`, `comments_count`) VALUES
(21, 'France', 'photo1.jpg', 'Effel Tower', 1461783681, 1),
(22, 'Ireland', 'photo2.jpg', 'The best view of Ireland', 1461783682, 0),
(23, 'London', 'photo3.jpg', 'London Eye', 1461783683, 1),
(24, 'Australia', 'photo4.jpg', 'Sydney Opera House', 1461783684, 1),
(25, 'America', 'photo5.jpg', 'Statue of Liberty', 1461783685, 0),
(26, 'Korea', 'photo6.jpg', 'Beautiful season', 1461783686, 0),
(27, 'Japan', 'photo7.jpg', 'Sakura', 1461783687, 0),
(28, 'China', 'photo8.jpg', 'The Great Wall', 1461783688, 0),
(29, 'ThaiLand', 'photo9.jpg', 'Sea', 1461783689, 0),
(30, 'Italy', 'photo10.jpg', 'Rome Colosseum', 1461783690, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_uploads`
--

CREATE TABLE `tbl_uploads` (
  `id` int(10) NOT NULL,
  `file` varchar(100) NOT NULL,
  `type` varchar(10) NOT NULL,
  `size` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_uploads`
--

INSERT INTO `tbl_uploads` (`id`, `file`, `type`, `size`) VALUES
(1, '70676-download.jpg', 'image/jpeg', 22);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(8) NOT NULL,
  `name` varchar(30) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`) VALUES
(2, 'bohyun', 'bohyun521@gmail.com', '275385a8ef4479bfa22c71f547644f90'),
(3, 'bob', 'bob@gmail.com', 'cd780d2cf8b7247cc50404d314a84743');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `add_delete_record`
--
ALTER TABLE `add_delete_record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s281_items_cmts`
--
ALTER TABLE `s281_items_cmts`
  ADD PRIMARY KEY (`c_id`),
  ADD KEY `c_item_id` (`c_item_id`);

--
-- Indexes for table `s281_photos`
--
ALTER TABLE `s281_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_uploads`
--
ALTER TABLE `tbl_uploads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `add_delete_record`
--
ALTER TABLE `add_delete_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `info`
--
ALTER TABLE `info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;
--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s281_items_cmts`
--
ALTER TABLE `s281_items_cmts`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `s281_photos`
--
ALTER TABLE `s281_photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `tbl_uploads`
--
ALTER TABLE `tbl_uploads`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
