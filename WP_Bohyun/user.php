<?php
error_reporting(E_COMPILE_ERROR);

require_once("ajax_table.class.php");
$obj = new ajax_table();
$records = $obj->getRecords();
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>User</title>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
        <script src="js/menu.js" type="text/javascript"></script>
        <script src="js/jquery-1.11.0.min.js" type="text/javascript"></script>
        <script src="js/jquery-ui.js" type="text/javascript"></script>
        <script src="js/table.js" type="text/javascript"></script>
        <link href="css/table.css" rel="stylesheet" type="text/css"/>
        <script>
            var columns = new Array("fname", "lname", "tech", "email", "address");
            var placeholder = new Array("Enter First Name", "Enter Last Name", "Enter Technology", "Enter Email", "Enter Address");
            var inputType = new Array("text", "text", "text", "text", "textarea");
            var table = "tableDemo";

            var savebutton = "ajaxSave";
            var deletebutton = "ajaxDelete";
            var editbutton = "ajaxEdit";
            var updatebutton = "ajaxUpdate";
            var cancelbutton = "cancel";

            var saveImage = "img/save.png"
            var editImage = "img/edit.png"
            var deleteImage = "img/remove.png"
            var cancelImage = "img/back.png"
            var updateImage = "img/save.png"

            var saveAnimationDelay = 3000;
            var deleteAnimationDelay = 1000;

            var effect = "flash";

        </script>
    </head>
    <body>
        <table border="0" class="tableDemo bordered">
            <tr class="ajaxTitle">
                <th width="2%">Sr</th>
                <th width="16%">First Name</th>
                <th width="16%">Last Name</th>
                <th width="12%">Technology</th>
                <th width="20%">Email</th>
                <th width="18%">Address</th>
                <th width="14%">Action</th>
            </tr>
            <?php
            if (count($records)) {
                $i = 1;
                foreach ($records as $key => $eachRecord) {
                    ?>
                    <tr id="<?= $eachRecord['id']; ?>">
                        <td><?= $i++; ?></td>
                        <td class="fname"><?= $eachRecord['fname']; ?></td>
                        <td class="lname"><?= $eachRecord['lname']; ?></td>
                        <td class="tech"><?= $eachRecord['tech']; ?></td>
                        <td class="email"><?= $eachRecord['email']; ?></td>
                        <td class="address"><?= $eachRecord['address']; ?></td>
                        <td>
                            <a href="javascript:;" id="<?= $eachRecord['id']; ?>" class="ajaxEdit"><img src="" class="eimage"></a>
                            <a href="javascript:;" id="<?= $eachRecord['id']; ?>" class="ajaxDelete"><img src="" class="dimage"></a>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </table>  





        <div class="main">
            <p title="Join Us" style="top:200px;left:120px;">
                <a href="logout.php">Log Out</a>
                <a href="Home.html">Home</a>
            </p>
            <p title="Explored" class="openTop blue" style="top:200px;left:260px;">
                <a href="explored.php">Explored</a> <br>
                <a href="map.html">Map</a> <br>
                <a href="rating.php">Rating</a><br>
                <a href="view_image.php">Image</a>
                <a href="user.php">User</a>
            </p>
            <p title="Contact Us" class="openTop red" style="top:200px;left:-30px;">
                <a href="comment.php">Leave Us a Comment</a>
            </p>
        </div>
         
        <script src="js/script.js" type="text/javascript"></script>
       
    </body>
</html>
