<?php
include_once './model/dbconfig.php';

?>
<!DOCTYPE html>
<html>
<head>
<title>File Uploading</title>
<link href="css/image.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="header">
<label>File Uploading</label>
</div>
<div id="body">
	<table width="80%" border="1">
    <tr>
        <th colspan="4">your uploads...<label><a href="image.php">upload new files...</a></label></th>
    </tr>
    <tr>
    <td>File Name</td>
    <td>File Type</td>
    <td>File Size(KB)</td>
    <td>View</td>
    </tr>
    <?php
	$sql="SELECT * FROM tbl_uploads";
	$result_set=mysql_query($sql);
	while($row=mysql_fetch_array($result_set))
	{
		?>
        <tr>
        <td><?php echo '<img src="uploads/'.$row['file'].'" height="40" width="35">' ?></td>
        <td><?php echo $row['type'] ?></td>
        <td><?php echo $row['size'] ?></td>
        <td><a href="uploads/<?php echo $row['file'] ?>" target="_blank">view file</a></td>
        </tr>
        <?php
	}
	?>
    </table>
    
</div>
</body>
</html>
