<?php
include_once './model/dbconfig.php';
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>File Uploading</title>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
        <script src="js/menu.js" type="text/javascript"></script>
        <link href="css/image.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="navigation">
            <ul class="nav">
                <li>
                    <a href="Home.html">Join Us</a>
                    <ul>
                        <li><a href="logout.php">Log Out</a></li>
                    </ul>
                </li>
                <li>
                    <a href="explored.php">Explored</a>
                    <ul>
                        <li><a href="map.html">Map</a></li>
                        <li><a href="rating.php">Rating</a></li>
                        <li><a href="view_image.php">Image</a></li>
                        <li><a href="user.php">User</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Contact Us</a>
                    <ul>
                        <li><a href="comment.php">Leave Us a Comment</a></li>
                    </ul>
                </li>
            </ul>
        </div>

        <div id="header">
            <label>File Uploading</label>
        </div>
        <div id="body">
            <form action="upload.php" method="post" enctype="multipart/form-data">
                <input type="file" name="file" />
                <button type="submit" name="btn-upload">upload</button>
            </form>
            <br />
            <?php
            if (isset($_GET['success'])) {
                ?>
                <label>File Uploaded Successfully...  <a href="view_image.php">click here to view file.</a></label>
                <?php
            } else if (isset($_GET['fail'])) {
                ?>
                <label>Problem While File Uploading !</label>
                <?php
            } else {
                ?>
                <label>Try to upload any files(PDF, DOC, EXE, VIDEO, MP3, ZIP,etc...)</label>
                <?php
            }
            ?>
        </div>
    </body>
</html>
