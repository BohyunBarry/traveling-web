<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
        <script src="js/menu.js" type="text/javascript"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#FormSubmit").click(function (e) {
                    e.preventDefault();
                    if ($("#contentText").val() === '')
                    {
                        alert("Please enter some text!");
                        return false;
                    }

                    $("#FormSubmit").hide();
                    $("#LoadingImage").show();

                    var myData = 'content_txt=' + $("#contentText").val();
                    jQuery.ajax({
                        type: "POST",
                        url: "response.php",
                        dataType: "text",
                        data: myData,
                        success: function (response) {
                            $("#responds").append(response);
                            $("#contentText").val('');
                            $("#FormSubmit").show();
                            $("#LoadingImage").hide();

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#FormSubmit").show();
                            $("#LoadingImage").hide();
                            alert(thrownError);
                        }
                    });
                });
                $("body").on("click", "#responds .del_button", function (e) {
                    e.preventDefault();
                    var clickedID = this.id.split('-');
                    var DbNumberID = clickedID[1];
                    var myData = 'recordToDelete=' + DbNumberID;

                    $('#item_' + DbNumberID).addClass("sel");
                    $(this).hide();

                    jQuery.ajax({
                        type: "POST",
                        url: "response.php",
                        dataType: "text",
                        data: myData,
                        success: function (response) {
                            $('#item_' + DbNumberID).fadeOut();
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(thrownError);
                        }
                    });
                });

            });
        </script>
        <link href="css/comment.css" rel="stylesheet" type="text/css"/>
        <title>Comment</title>
    </head>
    <div class="navigation">
            <ul class="nav">
                 <li>
                    <a href="explored.php">Explored</a>
                    <ul>
                        <li><a href="map.html">Map</a></li>
                        <li><a href="rating.php">Rating</a></li>
                        <li><a href="view_image.php">Image</a></li>
                        <li><a href="user.php">User</a></li>
                    </ul>
                </li>
                <li>
                    <a href="Home.html">Join Us</a>
                    <ul>
                        <li><a href="register.php">Sign up</a></li>
                        <li><a href="login.php">Log In</a></li>
                        <li><a href="logout.php">Log Out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    
    
    
    
    <div class="content_wrapper">
        <ul id="responds">
            <?php
            $username = "wp_db";
            $password = "wp_db";
            $hostname = "localhost";
            $databasename = 'wp_db';

            $mysqli = new mysqli($hostname, $username, $password, $databasename);
            
            $results = $mysqli->query("SELECT id,content FROM add_delete_record");
            while ($row = $results->fetch_assoc()) {
                echo '<li id="item_' . $row["id"] . '">';
                echo '<div class="del_wrapper"><a href="#" class="del_button" id="del-' . $row["id"] . '">';
                echo '<img src="img/icon_del.gif" border="0" />';
                echo '</a></div>';
                echo $row["content"] . '</li>';
            }
            $mysqli->close();
            ?>
        </ul>
        <div class="form_style">
            <textarea name="content_txt" id="contentText" cols="45" rows="5" placeholder="Enter some text"></textarea>
            <button id="FormSubmit">Add record</button>
            <img src="img/loading.gif" id="LoadingImage" style="display:none" />
        </div>
    </div>
</html>
