<?php

if (version_compare(phpversion(), "5.3.0", ">=")  == 1)
  error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
else
  error_reporting(E_ALL & ~E_NOTICE);


require_once('classes/CMySQL.php');
require_once('classes/CMyComments.php');

if ($_POST['action'] == 'accept_comment') {
    $action = filter_input(INPUT_POST, 'action');
    echo $GLOBALS['MyComments']->acceptComment();
    exit;
}

$sPhotos = '';
$aItems = $GLOBALS['MySQL']->getAll("SELECT * FROM `s281_photos` ORDER by `when` ASC");
foreach ($aItems as $i => $aItemInfo) {
    $sPhotos .= '<div class="photo"><img src="images/thumb_' . $aItemInfo['filename'] . '" id="' . $aItemInfo['id'] . '" /><p>' . $aItemInfo['title'] . ' item</p><i>' . $aItemInfo['description'] . '</i></div>';
}
?>


<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Explored</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
        <script src="js/menu.js" type="text/javascript"></script>
        <script src="js/comments.js" type="text/javascript"></script>
        
        <link href="css/comments.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="navigation">
            <ul class="nav">
                <li>
                    <a href="Home.html">Join Us</a>
                    <ul>
                        <li><a href="login.php">Log In</a></li>
                        <li><a href="logout.php">Log Out</a></li>
                    </ul>
                </li>
                <li>
                    <a href="explored.php">Explored</a>
                    <ul>
                        <li><a href="map.html">Map</a></li>
                        <li><a href="rating.php">Rating</a></li>
                        <li><a href="view_image.php">Image</a></li>
                        <li><a href="user.php">User</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Contact Us</a>
                    <ul>
                        <li><a href="comment.php">Leave Us a Comment</a></li>
                    </ul>
                </li>
            </ul>
        </div>




        <div class="container">
            <?= $sPhotos ?>
        </div>

        <div id="photo_preview" style="display:none">
            <div class="photo_wrp">
                <img class="close" src="images/close.gif" />
                <div style="clear:both"></div>
                <div class="pleft">test1</div>
                <div class="pright">test2</div>
                <div style="clear:both"></div>
            </div>
        </div>
    </body>
</html>
